//
//  SchoolDirectoryTableViewController.swift
//  20200316-IlqarIlyasov-NYCSchools
//
//  Created by Ilqar Ilyasov on 3/17/20.
//  Copyright © 2020 IIlyasov. All rights reserved.
//

import UIKit

class SchoolDirectoryTableViewController: UITableViewController {

    // MARK: - Properties

    private let navTitle = "High School Directory"
    private let directoryCellID = "HighSchoolCell"
    private let showDetailSegueID = "ShowDetail"

    private let networkController = NetworkController<[HighSchool]>()
    private var schoolDir: [HighSchool]? {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }


    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navTitle

        networkController.getHighSchoolDirectory { (schoolDir, error) in
            if let error = error {
                // We should handle error here by presenting an alert to the user
                NSLog("Error getting high school directory: \(error)")
                return
            }

            if let _schools = schoolDir {
                self.schoolDir = _schools
            }
        }
    }


    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /// If network call haven't done yet this will create only one cell for Loading... text
        if schoolDir == nil {
            return 1
        }

        /// If schoolDir is not nil, then we will create cells based on its count
        return schoolDir!.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: directoryCellID, for: indexPath)

        /// If network call haven't done yet, only Loading... will appear on the screen
        if schoolDir == nil {
            cell.textLabel?.text = "Loading ..."
            return cell
        }

        /// Otherwise we will use schoolDir's names
        cell.textLabel?.text = schoolDir?[indexPath.row].name
        return cell
    }


    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showDetailSegueID {
            guard let destVC = segue.destination as? SchoolDetailTableViewController,
                let indexPath = tableView.indexPathForSelectedRow else { return }
            destVC.networkController = networkController
            destVC.schoolDetail = schoolDir?[indexPath.row]
            destVC.dbn = schoolDir?[indexPath.row].dbn
        }
    }
}
