//
//  SchoolDetailTableViewController.swift
//  20200316-IlqarIlyasov-NYCSchools
//
//  Created by Ilqar Ilyasov on 3/17/20.
//  Copyright © 2020 IIlyasov. All rights reserved.
//

import UIKit

class SchoolDetailTableViewController: UITableViewController {

    // MARK: - Note
    // Hello there.
    // Just want mention that, I could create more sophisticated UI for detail screen using a mix of collection view
    // and custom XIBs, as well as programmatic views.
    // But I'm working full time and taking CS classes after work. I'm sorry that I don't have more time to work on this.
    // But I worked on other projects that you can check out from AppStore:
    // • One Prayer - https://apps.apple.com/us/app/one-prayer/id1456578529
    // • My GTT - https://apps.apple.com/us/app/my-gtt/id1327942648
    // • Mi Movistar Uruguay - https://apps.apple.com/us/app/mi-movistar-uy/id785193700
    // • Mi Claro Argentina - https://apps.apple.com/ar/app/mi-claro-argentina/id1188491901


    // MARK: - Properties

    private let navTitle = "School Detail"
    private let detailCellID = "SchoolDetailCell"
    var networkController: NetworkController<[HighSchool]>!

    var schoolDetail: HighSchool? {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    /// As soon as we pass dbn value from SchoolDirectoryTVC to here,
    /// we are making another network call to get SAT results
    var dbn: String? {
        didSet {
            networkController.getSATResults(dbn: dbn!) { (detail, error) in
                if let error = error {
                    // We should handle error here by presenting an alert to the user
                    NSLog("Error getting SAT results: \(error)")
                    return
                }

                /// After getting SAT results we simply update our existing HighScool object
                if let _detail = detail?.first {
                    self.addSATDetail(to: &self.schoolDetail, from: _detail)
                }
            }
        }
    }


    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navTitle
    }


    // MARK: - Utility methods

    /// Takes an existing and a new HighScool object and add SAT values from new to the excisting object
    private func addSATDetail(to school: inout HighSchool?, from: HighSchool){
        school?.numOfSATtakers = from.numOfSATtakers
        school?.satWritingAvgScore = from.satWritingAvgScore
        school?.satMathAvgScore = from.satMathAvgScore
        school?.satCriticalReadingAvgScore = from.satCriticalReadingAvgScore
    }


    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /// If network call haven't done yet this will create only one cell for Loading... text
        if schoolDetail == nil {
            return 1
        }

        /// If schoolDetail is not nil, then we will create cells based on its count
        return schoolDetail!.arrayRepresentation.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: detailCellID, for: indexPath)

        /// If network call haven't done yet, only Loading... will appear on the screen
        if schoolDetail == nil {
            cell.textLabel?.text = "Loading ..."
            return cell
        }

        /// Otherwise we will use schoolDetail's arrayRepresentation
        cell.textLabel?.text = schoolDetail?.arrayRepresentation[indexPath.row]
        return cell
    }
}
