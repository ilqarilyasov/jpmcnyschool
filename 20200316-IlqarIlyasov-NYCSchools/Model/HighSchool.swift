//
//  HighSchool.swift
//  20200316-IlqarIlyasov-NYCSchools
//
//  Created by Ilqar Ilyasov on 3/16/20.
//  Copyright © 2020 IIlyasov. All rights reserved.
//

import Foundation

struct HighSchool: Codable {
    let dbn: String
    let name: String
    let overview: String?
    let address: String?
    let email: String?
    let phoneNumber: String?
    let website: String?
    let totalStudents: String?
    var numOfSATtakers: String?
    var satWritingAvgScore: String?
    var satMathAvgScore: String?
    var satCriticalReadingAvgScore: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case overview = "overview_paragraph"
        case address = "primary_address_line_1"
        case email = "school_email"
        case phoneNumber = "phone_number"
        case website
        case totalStudents = "total_students"
        case numOfSATtakers = "num_of_sat_test_takers"
        case satWritingAvgScore = "sat_writing_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
    }

    /// This computed property is being used to populate detail table of any selected high school.
    /// We can also use throwing Decodable initializer and create different representations or properties as well.
    var arrayRepresentation: [String] {
        return [name,
                overview ?? "---",
                "Address: \(address ?? "---")",
                "Website: \(website ?? "---")",
                "Email: \(email ?? "---")",
                "Phone: \(phoneNumber ?? "---")",
                "Total Students: \(totalStudents ?? "---")",
                "Num of SAT test takers: \(numOfSATtakers ?? "---")",
                "SAT Writing (avg): \(satWritingAvgScore ?? "---")",
                "SAT Math (avg): \(satMathAvgScore ?? "---")",
                "SAT Critical Reading (avg): \(satCriticalReadingAvgScore ?? "---")"]
    }
}
