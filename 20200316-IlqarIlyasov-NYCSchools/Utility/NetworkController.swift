//
//  NetworkController.swift
//  20200316-IlqarIlyasov-NYCSchools
//
//  Created by Ilqar Ilyasov on 3/16/20.
//  Copyright © 2020 IIlyasov. All rights reserved.
//

import Foundation

class NetworkController<Element: Codable> {

    // MARK: - Properties

    private let appToken = "L2Y2bgBBzhG2cwwSM3BrouTd2"
    private let contentType = "application/json"
    private(set) var schoolDirEndpoint = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
    private(set) var satResultsEndpoint = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!


    // MARK: - Get methods

    func getHighSchoolDirectory(completion: @escaping (Element?, Error?) -> Void) {

        /// Instead of getting all information we're filtering it using SoQL
        /// More: https://dev.socrata.com/docs/queries/
        let selectValues = "dbn, school_name, overview_paragraph, primary_address_line_1, school_email, website, phone_number, total_students"
        let queryItems = [URLQueryItem(name: "$select", value: selectValues),
                          URLQueryItem(name: "$order", value: "school_name ASC")]
        let request = constructURLRequest(endpoint: schoolDirEndpoint, queryItems: queryItems)
        makeURLRequest(request, completion)
    }

    func getSATResults(dbn: String, completion: @escaping (Element?, Error?) -> Void) {
        let queryItems = [URLQueryItem(name: "dbn", value: dbn)]
        let request = constructURLRequest(endpoint: satResultsEndpoint, queryItems: queryItems)
        makeURLRequest(request, completion)
    }


    // MARK: - Utility methods

    private func constructURLRequest(endpoint: URL, queryItems: [URLQueryItem]?) -> URLRequest {
        var comp = URLComponents(url: endpoint, resolvingAgainstBaseURL: true)!
        comp.queryItems = queryItems

        var request = URLRequest(url: comp.url!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.addValue(appToken, forHTTPHeaderField: "app_token")
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        return request
    }

    /// We can also use custom Error enum
    private func makeURLRequest(_ request: URLRequest, _ completion: @escaping (Element?, Error?) -> Void) {
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                NSLog("URLSession. Error: \(error)\nLine: \(#line)\nFile: \(#file)")
                completion(nil, error)
                return
            }

            guard let data = data else {
                NSLog("URLSession. Error: no data returned.\nLine: \(#line)\nFile: \(#file)")
                completion(nil, NSError(domain: "kCustomErrorDomain", code: 001))
                return
            }

            do {
                let result = try JSONDecoder().decode(Element.self, from: data)
                completion(result, nil)
            } catch {
                NSLog("JSONDecoder. Error decoding data.\nLine: \(#line)\nFile: \(#file)")
                completion(nil, error)
            }
        }.resume()
    }
}
