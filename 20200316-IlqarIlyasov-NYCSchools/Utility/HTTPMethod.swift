//
//  HTTPMethod.swift
//  20200316-IlqarIlyasov-NYCSchools
//
//  Created by Ilqar Ilyasov on 3/16/20.
//  Copyright © 2020 IIlyasov. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}
